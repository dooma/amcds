syntax = "proto3";

/* 1. Main algorithms to implement:
      - Uniform reliable broadcast (URB): algorithm 3.4 on page 83
      - (1,N) atomic register (ONAR): algorithm 4.5 on page 157
      - Uniform consensus (UC): algorithm 5.3 on page 213

   2. Lower abstractions to implement:
      - Best effort broadcast: algorithm 3.1 on page 76
      - Perfect failure detector: algorithm 2.5 on page 51

   3. Use TCP as Perfect Link.

   4. Implement a single program running all three algorithms. The abstrations stack will look
      similar to the one below, except that only ONAR uses PL directly.

            _____APP - - - - - - - - - - APP_____
           |      |                       |      \
           | URB,ONAR,UC - - - - - - URB,ONAR,UC |
           |    / | \                   / |  \   |
           |  PFD |  BEB - - - - - -  BEB |  PFD |
           |    \ | /                   \ | /    |
           \_____PL                      PL_____/
                  \___CLIENT/SWITCH_____/

    5. Every communication will be sent to the switch or will be received from the switch.

    6. Certain messages that are not sent over the network by the algorithm will be sent to
       the reporting ip:port

    7. Configure the command for your process in dalg.conf.xml

    8. A process is identified by a process id (PID) of the form IP:PORT

    9. Your program should expect the as command line arguments, a list of PIDs in the
       following order:
       - Switch PID
       - Reporting PID
       - PID to be used by the process being started
       - PIDs of all the remaining processes in the system

   10. Implementation provided by the professor:
       - dalg-process - process implementation
       - dalg-switch - switch and also testing client implementation
       - dalg-launcher - will launch your impementation and any other you specify in dalg.conf.xml

   11. The APP abstractions is not part of the algorithms in the book and it is meant to
       trigger the abstractions. The perfect link should receive (from the switch) Application*
       messages and pass all of them to the APP abstraction

   12. Events trigger by messages will get associated a message named
       <AbstractionAcronym><EventName>, for instance urb.Broadcast -> UrbBroadcast. Events that
       do not have a name, but are used in a certain abstraction and have a type will have a
       message associated with them named <AbstractionAcronym><Type>, for instance UrbData. The
       init events will not have such messages. The events triggered by a condition being true,
       will be implemented by calling the handler's code whereever that condition is changed.

   13. Each Message will have a unique ID composed of the IP and PORT of the process and the
       value of a global static counter, as in IP:PORT:COUNT

   14. The switch will open two TCP connections to each process. The first will me used for
       messages from the switch to the process, and the second one for the other direction.

   15. Messages traveling through the wire
       - process -> switch: Message(PlSend(Message(...)))
       - switch -> process: Message(SwitchWrapper(Message(PlSend(Message(...)))))
       - process -> report: Message(UrbDeliver)
                            Message(OnarReadReturn)
                            Message(OnarWriteReturn)
                            Message(UcDecide)
                            Message(BebDeliver)
                            Message(PfdCrash)
                            Message(PlDeliver)

   16. The communication is done using the Google Protobuffer 3.x messages defined below, over
       TCP. The exchange will be asynchronous, through the two sockets opened by the switch to
       each process.

   17. The evaluation will be done as follows:
       - Your laptops will connect to a small wired (or wireless) switch and have predefined
         static addresses
       - The instructor's laptop will also be connected to the switch and test your nodes
         using the switch running on the instructor's laptop
       - You will run 4 nodes on your laptop

   18. When sending a message through the socket, send first a 32-bit int in network byte
       order with the length of the serialized message, followed by the serialized message.
       When reading from the socket, expect the same.

   19. The switch will restart all processes before testing another algorithm, so you can
       determine the abstraction stack you are using from the Application* message you
       receive. This will help route upward events to the appropriate abstractions
*/

// Application messages
message ApplicationBroadcast { // Should trigger a UrbBroadcast
    string value = 1;
}

message ApplicationRead { // Should trigger an OnarRead
}

message ApplicationWrite { // Should trigger an OnarWrite
    string value = 1;
}

message ApplicationPropose { // Should trigger a UcPropose
    string value = 1;
}

// URB messages
message UrbBroadcast {
    Message message = 1;
}

message UrbData {
    string sourcePid = 1;
    Message message = 2;
}

message UrbDeliver { // Should be reported wrapped in a Message
    string sourcePid = 1;
    Message message = 2;
}

// ONAR messages
message OnarRead {
}

message OnarReadReturn { // Should be reported wrapped in a Message
    Message value = 1;
}

message OnarWrite {
    Message value = 1;
}

message OnarWriteReturn { // Should be reported wrapped in a Message
}

message OnarInternalWrite {
    int32 timestamp = 1;
    Message value = 2;
}

message OnarAck {
}

// UC messages
message UcPropose {
    Message value = 1;
}

message UcProposal {
    Message value = 1;
    int32 round = 2;
    repeated Message proposalSet = 3;
}

message UcDecide { // Should be reported wrapped in a Message
    Message decision = 1;
}

// BEB messages
message BebBroadcast {
    Message message = 1;
}

message BebDeliver { // Should be reported wrapped in a Message
    string senderPid = 1;
    Message message = 2;
}

// PFD messages
message PfdHeartbeatRequest {
}

message PfdHeartbeatReply {
}

message PfdCrash { // Should be reported wrapped in a Message
    string pid = 1;
}

// PL messages
message PlSend {
    string destinationPid = 1;
    Message message = 2;
}

message PlDeliver { // Should be reported wrapped in a Message
    string senderPid = 1;
    Message message = 2;
}

message SwitchWrapper {   // Every message received by the switch will be wrapped in a
    string senderPid = 1; // Message containing a SwitchWrapper, and forwarded to the
    Message message = 2;  // destination process. This is for the sole purpose of having
}                         // the sender process stored somewhere.

message Message {
    enum Type {
        PAYLOAD = 0;

        APPLICATION_BROADCAST = 10;
        APPLICATION_READ = 11;
        APPLICATION_WRITE = 12;
        APPLICATION_PROPOSE = 13;

        URB_BROADCAST = 20;
        URB_DATA = 21;
        URB_DELIVER = 22;

        ONAR_READ = 30;
        ONAR_READ_RETURN = 31;
        ONAR_WRITE = 32;
        ONAR_WRITE_RETURN = 33;
        ONAR_INTERNAL_WRITE = 34;
        ONAR_ACK = 35;

        UC_PROPOSE = 40;
        UC_PROPOSAL = 41;
        UC_DECIDE = 42;

        BEB_BROADCAST = 50;
        BEB_DELIVER = 51;

        PFD_HEARTBEAT_REQUEST = 60;
        PFD_HEARTBEAT_REPLY = 61;
        PFD_CRASH = 62;

        PL_SEND = 70;
        PL_DELIVER = 71;

        SWITCH_WRAPPER = 1000;
    }

    Type type = 1;
    string messageId = 2;
    string payload = 3;

    ApplicationBroadcast applicationBroadcast = 10;
    ApplicationRead applicationRead = 11;
    ApplicationWrite applicationWrite = 12;
    ApplicationPropose applicationPropose = 13;

    UrbBroadcast urbBroadcast = 20;
    UrbData urbData = 21;
    UrbDeliver urbDeliver = 22;

    OnarRead onarRead = 30;
    OnarReadReturn onarReadReturn = 31;
    OnarWrite onarWrite = 32;
    OnarWriteReturn onarWriteReturn = 33;
    OnarInternalWrite onarInternalWrite = 34;
    OnarAck onarAck = 35;

    UcPropose ucPropose = 40;
    UcProposal ucProposal = 41;
    UcDecide ucDecide = 42;

    BebBroadcast bebBroadcast = 50;
    BebDeliver bebDeliver = 51;

    PfdHeartbeatRequest pfdHeartbeatRequest = 60;
    PfdHeartbeatReply pfdHeartbeatReply = 61;
    PfdCrash pfdCrash = 62;

    PlSend plSend = 70;
    PlDeliver plDeliver = 71;

    SwitchWrapper switchWrapper = 1000;
}
