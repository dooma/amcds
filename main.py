import asyncio
import sys
import time
import signal
from collections import namedtuple
from multiprocessing import Process

from algorithms.pfd import PerfectFailureDetector
from algorithms.pl import PerfectLink, Server
from algorithms.beb import BestEffortBroadcast
from algorithms.urb import UniformReliableBroadcast
from algorithms.onar import AtomicRegister
from algorithms.uc import UniformConsensus
from stress import Stress
from algorithms.urb_pb2 import Message
import logging as logger

from pubsub import pub

logger.basicConfig(level=logger.WARN, format='%(threadName)s %(message)s')


class Structure(namedtuple('Structure', ['host', 'port'])):
    def __str__(self):
        return ':'.join((self.host, str(self.port)))

def signal_handler(signal, frame):
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

prompt = """
broadcast - URB BROADCAST
read - ONAR READ
write - ONAR WRITE
propose - PROPOSE
exit - EXIT APPLICATION
>"""

actions = {
    'broadcast': ('urb_broadcast', str),
    'read': ('onar_read', None),
    'write': ('onar_write', int),
    'propose': ('uc_propose', int),
    'exit': ('exit', None),
}

class Application(Process):
    def __init__(self, connection_details, others, call):
        self.connection_details = connection_details
        self.others = others
        self.beb = BestEffortBroadcast(connection_details, others)
        self.call = call
        super(Application, self).__init__()
        pub.subscribe(self.urb_deliver, str(self.connection_details) + str(Message.Type.Name(Message.URB_DELIVER)))
        pub.subscribe(self.onar_read_return, str(self.connection_details) + str(Message.Type.Name(Message.ONAR_READ_RETURN)))
        pub.subscribe(self.onar_write_return, str(self.connection_details) + str(Message.Type.Name(Message.ONAR_WRITE_RETURN)))
        pub.subscribe(self.uc_decide, str(self.connection_details) + str(Message.Type.Name(Message.UC_DECIDE)))
    
    def run(self):
        http_server = Server(self.connection_details)
        http_server.start()
        logger.info('TCP Server started on {}'.format(self.connection_details))

        time.sleep(1)

        pfd = PerfectFailureDetector(self.connection_details, self.others)
        pfd.start()
        logger.info('PFD started')

        method_to_call = getattr(self, self.call[0])
        value = self.call[1]

        method_to_call(value)

        pfd.join()
        http_server.join()

    def urb_broadcast(self, value):
        self.urb = UniformReliableBroadcast(self.connection_details, self.others)
        message = Message()
        message.type = Message.PAYLOAD
        message.payload = value
        self.urb.broadcast(message)
        logger.info('URB Broadcast {} request with payload: {}'.format(self.connection_details, message.payload))

    def onar_read(self, value):
        self.onar = AtomicRegister(self.connection_details, self.others)
        self.onar.read()
        logger.info('ONAR Read {} request'.format(self.connection_details))

    def onar_write(self, value):
        self.onar = AtomicRegister(self.connection_details, self.others)
        self.onar.write(value)
        logger.info('ONAR Write {} request with payload: {}'.format(self.connection_details, 1))

    def uc_propose(self, value):
        self.uc = UniformConsensus(self.connection_details, self.others)
        self.uc.propose(value)
        logger.info('UC Propose {} request with payload: {}'.format(self.connection_details, 1))

    def urb_deliver(self, process, message):
        print('URB DELIVER with: {}'.format(message))
        self.exit_process()

    def onar_read_return(self, message):
        print('ONAR READ RETURN with: {}'.format(message))
        self.exit_process()
    
    def onar_write_return(self):
        print('ONAR WRITE RETURN')
        self.exit_process()

    def uc_decide(self, decision):
        print('UC DECISION with: {}'.format(decision))
        self.exit_process()

    def exit_process(self):
        sys.exit(0)
        
if __name__ == "__main__":
    if len(sys.argv) < 4:
        sys.exit('Please provide:\n-switch PID\n-report PID\n-current process PID\n- <n> PID for all remaining processes')
    
    pids = (pid.split(':') for pid in sys.argv[1:])

    switch, report, *apps = (Structure(host, int(port)) for host, port in pids)

    while True:
        command = input(prompt)
        try:
            method, parameter = actions[command]
            value = parameter(input('value >')) if parameter else None
        except Exception:
            logger.info('Invalid command')
            continue

        processes = []  
        for pid in apps:
            others = [other_pid for other_pid in apps if other_pid != pid]
            process = Application(pid, [other_pid for other_pid in apps if other_pid != pid], (method, value))
            process.start()
            processes.append(process)

        for process in processes:
            process.join()
