from algorithms.urb_pb2 import Message, UrbBroadcast, UrbData, UrbDeliver
from collections import defaultdict

from pubsub import pub
import logging as logger

logger.basicConfig(level=logger.DEBUG, format='%(threadName)s %(message)s')


class UniformReliableBroadcast():
    def __init__(self, pid, others):
        self.pid = pid
        self.delivered = []
        self.pending = []
        self.correct = others
        self.ack = defaultdict(list)
        pub.subscribe(self.broadcast, str(self.pid) + str(Message.Type.Name(Message.URB_BROADCAST)))
        pub.subscribe(self.beb_deliver, str(self.pid) + str(Message.Type.Name(Message.BEB_DELIVER)))
        pub.subscribe(self.crash, str(self.pid) + str(Message.Type.Name(Message.PFD_CRASH)))

    def broadcast(self, message):
        request = UrbData()
        request.sourcePid = str(self.pid)
        request.message.CopyFrom(message)
        message = Message()
        message.type = Message.URB_DATA
        message.urbData.CopyFrom(request)
        self.pending.append((str(self.pid), message.SerializeToString()))
        logger.info('URB {} emit BEB BROADCAST'.format(self.pid))
        pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message = message)

    def beb_deliver(self, process, message):
        if message.type != Message.URB_DATA:
            return
        logger.info('URB {} got BEB DELIVER'.format(self.pid))
        self.ack[message.SerializeToString()].append(process)
        if (process, message.SerializeToString()) not in self.pending:
            self.pending.append((process, message.SerializeToString()))
            logger.info('URB {} emit BEB BROADCAST after BEB DELIVER'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message = message)
        if self.candeliver(message) and message.SerializeToString() not in self.delivered:
            self.delivered.append(message.SerializeToString())
            logger.info('URB {} emit URB DELIVER'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.URB_DELIVER)), process=process, message=message)

    def crash(self, process):
        print('URB PID {} got PFD_CRASH for {}'.format(self.pid, process))
        self.correct.remove(process)

    def candeliver(self, message):
        return all(str(process) in self.ack[message.SerializeToString()] for process in self.correct)