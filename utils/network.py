import struct
import sys

def pack_packet(message):
    message_size = sys.getsizeof(message)
    return struct.pack('L', message_size) + message

def unpack_packet(packet):
    size = *struct.unpack('>L', packet)
    return struct.unpack_from('')