from pubsub import pub
from algorithms.urb_pb2 import Message, OnarWrite, OnarRead, OnarAck
from algorithms.pl import PerfectLink
import logging as logger
from collections import namedtuple

logger.basicConfig(level=logger.DEBUG, format='%(threadName)s %(message)s')

class Structure(namedtuple('Structure', ['host', 'port'])):
    def __str__(self):
        return ':'.join((self.host, str(self.port)))

class AtomicRegister():
    def __init__(self, pid, others):
        self.pid = pid
        self.timestamp, self.val = 0, 0
        self.correct = others
        self.writeset = []
        self.readval = 0
        self.reading = False
        pub.subscribe(self.crash, str(self.pid) +  str(Message.Type.Name(Message.PFD_CRASH)))
        pub.subscribe(self.read, str(self.pid) + str(Message.Type.Name(Message.ONAR_READ)))
        pub.subscribe(self.write, str(self.pid) + str(Message.Type.Name(Message.ONAR_WRITE)))
        pub.subscribe(self.beb_deliver, str(self.pid) + str(Message.Type.Name(Message.BEB_DELIVER)))
        pub.subscribe(self.pl_deliver, str(self.pid) + str(Message.Type.Name(Message.PL_DELIVER)))
        pub.subscribe(self.onar_return, str(self.pid) + 'ONAR_RETURN')

    def read(self):
        self.reading = True
        self.readval = self.val
        payload = Message()
        payload.type = Message.PAYLOAD
        payload.payload = ':'.join([str(self.timestamp), str(self.val)])
        request = OnarWrite()
        request.value.CopyFrom(payload)
        message = Message()
        message.type = Message.ONAR_WRITE
        message.onarWrite.CopyFrom(request)
        logger.info('ONAR {} emit BEB BROADCAST from READ'.format(self.pid))
        pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message=message)

    def write(self, value):
        payload = Message()
        payload.type = Message.PAYLOAD
        payload.payload = ':'.join([str(self.timestamp), str(self.val)])
        request = OnarWrite()
        request.value.CopyFrom(payload)
        message = Message()
        message.type = Message.ONAR_WRITE
        message.onarWrite.CopyFrom(request)
        pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message=message)

    def beb_deliver(self, process, message):
        if message.type != Message.ONAR_WRITE:
            return
        logger.info('ONAR {} got BEB DELIVER'.format(self.pid))
        timestamp, val = [int(value) for value in message.onarWrite.value.payload.split(':')]
        if timestamp > self.timestamp:
            self.timestamp, self.val = timestamp, val

        pid = process.split(':')
        process = Structure(host=pid[0], port=int(pid[1]))
        request = OnarAck()
        message = Message()
        message.type = Message.ONAR_ACK
        message.onarAck.CopyFrom(request)
        PerfectLink.send(process=process, message=message, pid=self.pid)

    def pl_deliver(self, process, message):
        if message.type != Message.ONAR_ACK:
            return
        logger.info('ONAR {} got PL DELIVER'.format(self.pid))
        self.writeset.append(process)
        logger.info('ONAR {} emit ONAR RETURN'.format(self.pid))
        pub.sendMessage(str(self.pid) + 'ONAR_RETURN')

    def crash(self, process):
        logger.info('ONAR PID {} got PFD_CRASH for {}'.format(self.pid, process))
        self.correct.remove(process)

    def onar_return(self):
        logger.info('ONAR {} got ONAR RETURN'.format(self.pid))
        if not all(str(process) in self.writeset for process in self.correct):
            return
        self.writeset = []
        if self.reading:
            self.reading = False
            logger.info('ONAR {} emit ONAR READ RETURN'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.ONAR_READ_RETURN)), message=self.readval)
        else:
            logger.info('ONAR {} emit ONAR WRITE RETURN'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.ONAR_WRITE_RETURN)))
