import logging
import time
import uuid
from threading import Thread
import asyncio

from pubsub import pub

import algorithms.urb_pb2 as proto
from algorithms.pl import PerfectLink
from algorithms.urb_pb2 import Message, PfdHeartbeatRequest, PfdHeartbeatReply

SLEEP_TIMEOUT=5

class PerfectFailureDetector(Thread):
    def __init__(self, pid, others):
        Thread.__init__(self)
        self.pid = pid
        self.detected = []
        self.others = others
        self.alive = others
        pub.subscribe(self.hearthbeat_request, str(self.pid) + str(Message.Type.Name(Message.PL_DELIVER)))
        pub.subscribe(self.hearthbeat_reply, str(self.pid) + str(Message.Type.Name(Message.PL_DELIVER)))

    def run(self):
        while 1:
            self.timeout()
            time.sleep(SLEEP_TIMEOUT)

    def timeout(self):
        for process in self.others:
            if process not in self.alive and process not in self.detected:
                self.detected.append(process)
                pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.PFD_CRASH)), process=process)
            request = PfdHeartbeatRequest()
            message = Message()
            message.type = Message.PFD_HEARTBEAT_REQUEST
            message.messageId = ':'.join((str(self.pid), str(time.time())))
            message.pfdHeartbeatRequest.CopyFrom(request)
            PerfectLink.send(process, message, self.pid)
        self.alive = []

    def hearthbeat_request(self, process, message):
        if message.type != Message.PFD_HEARTBEAT_REQUEST:
            return
        sender_pid = process
        request = PfdHeartbeatReply()
        message = Message()
        message.type = Message.PFD_HEARTBEAT_REPLY
        message.messageId = ':'.join((str(self.pid), str(time.time())))
        message.pfdHeartbeatReply.CopyFrom(request)

        try:
            destination_pid = self.get_associated_pid(sender_pid)
            PerfectLink.send(destination_pid, message, self.pid)
        except StopIteration:
            pass

    def hearthbeat_reply(self, process, message):
        if message.type != Message.PFD_HEARTBEAT_REPLY:
            return
        try:
            self.alive.append(self.get_associated_pid(process))
        except StopIteration:
            pass

    def get_associated_pid(self, pid_str):
        return next(process for process in self.others if str(process) == pid_str)