from pubsub import pub

from algorithms.urb_pb2 import Message, UcDecide, UcProposal, UcPropose

import logging as logger

logger.basicConfig(level=logger.DEBUG, format='%(threadName)s %(message)s')


N = 10

class UniformConsensus():
    def __init__(self, pid, others):
        self.pid = pid
        self.correct = others
        self.round = 1
        self.decision = 0
        self.proposalset = []
        self.receivedfrom = []
        pub.subscribe(self.crash, str(self.pid) + str(Message.Type.Name(Message.PFD_CRASH)))
        pub.subscribe(self.propose, str(self.pid) + str(Message.Type.Name(Message.UC_PROPOSE)))
        pub.subscribe(self.beb_deliver, str(self.pid) + str(Message.Type.Name(Message.BEB_DELIVER)))
        pub.subscribe(self.decide, str(self.pid) + 'UC_INTERNAL_DECIDE')

    def crash(self, process):
        print('UC PID {} got PFD_CRASH for {}'.format(self.pid, process))
        self.correct.remove(process)
    
    def propose(self, value):
        logger.info('UC {} got UC PROPOSE with payload: {}'.format(self.pid, value))
        self.proposalset.append(value)
        request = UcProposal()
        request.round = self.round

        for value in self.proposalset:
            proposal_message = Message()
            proposal_message.type = Message.PAYLOAD
            proposal_message.payload = str(value)
            request.proposalSet.append(proposal_message)

        message = Message()
        message.type = Message.UC_PROPOSAL
        message.ucProposal.CopyFrom(request)
        pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message=message)

    def beb_deliver(self, process, message):
        if message.type != Message.UC_PROPOSAL:
            return
        logger.info('UC {} got BEB DELIVER'.format(self.pid))
        rnd, proposalset = message.ucProposal.round, [int(value.payload) for value in message.ucProposal.proposalSet]
        self.receivedfrom.append(process)
        self.proposalset.extend(proposalset)
        logger.info('UC {} emit UC INTERNAL DECIDE'.format(self.pid))
        pub.sendMessage(str(self.pid) + 'UC_INTERNAL_DECIDE')
    
    def decide(self):
        logger.info('UC {} got UC INTERNAL DECIDE'.format(self.pid))
        if not all(str(process) in self.receivedfrom for process in self.correct) or self.decision is None:
            return
        if self.round >= len(self.correct):
            self.decision = min(self.proposalset)
            logger.info('UC {} emit UC DECIDE'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.UC_DECIDE)), decision=self.decision)
        else:
            self.round += 1
            self.receivedfrom = []
            request = UcProposal()
            request.round = self.round
            message = Message()
            message.type = Message.UC_PROPOSAL
            message.ucProposal.CopyFrom(request)
            logger.info('UC {} emit BEB BROADCAST from decide'.format(self.pid))
            pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)), message=message)