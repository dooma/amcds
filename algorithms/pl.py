import asyncio
import socket
import socketserver
import time
import uuid
from threading import Thread

from algorithms.urb_pb2 import Message, PlSend, PlDeliver

from pubsub import pub

class PerfectLink():
    @classmethod
    def send(cls, process, message, pid):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                sock.connect((process.host, process.port))
            except Exception as exc:
                print('SEND FAILED: PID {} TO {} WITH: {}'.format(pid, process, exc))
                return

            request = PlSend()
            request.destinationPid = str(pid)
            request.message.CopyFrom(message)

            wrapper = Message()
            wrapper.type = Message.PL_SEND
            wrapper.messageId = ':'.join((str(process), str(time.time())))
            wrapper.plSend.CopyFrom(request)
            sock.send(bytes(bin(wrapper.ByteSize())[2:].zfill(32), 'utf-8'))
            sock.sendall(wrapper.SerializeToString())

    @classmethod
    def deliver(cls, to_pid, message):
        to_pid = [str(elem) for elem in to_pid]
        pub.sendMessage(':'.join(to_pid) + str(Message.Type.Name(Message.PL_DELIVER)), process=message.senderPid, message=message.message)

class Handler(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(32).strip()
        buffer_length = int(self.data, 2)
        self.data = self.request.recv(buffer_length).strip()

        message = Message()
        message.ParseFromString(self.data)

        response = PlDeliver()
        response.senderPid = message.plSend.destinationPid
        response.message.CopyFrom(message.plSend.message)
    
        PerfectLink.deliver(self.server.server_address, response)

class Server(Thread):
    def __init__(self, connection_details):
        self.pid = connection_details
        Thread.__init__(self)

    def run(self):
        try:
            with socketserver.TCPServer((self.pid.host, self.pid.port), Handler) as server:
                server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                server.serve_forever()
        except Exception as e:
            print('TCP Server failed to start on: ', self.pid)