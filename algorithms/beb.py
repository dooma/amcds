from algorithms.pl import PerfectLink
from algorithms.urb_pb2 import Message, BebBroadcast, BebDeliver
import time
from pubsub import pub


class BestEffortBroadcast():
    def __init__(self, pid, others):
        self.pid = pid
        self.others = others
        pub.subscribe(self.broadcast, str(self.pid) + str(Message.Type.Name(Message.BEB_BROADCAST)))
        pub.subscribe(self.deliver, str(self.pid) + str(Message.Type.Name(Message.PL_DELIVER)))

    def broadcast(self, message):
        for process in self.others:
            PerfectLink.send(process, message, self.pid)

    def deliver(self, process, message):
        pub.sendMessage(str(self.pid) + str(Message.Type.Name(Message.BEB_DELIVER)), process=process, message=message)

